/**
 * This package contains the classes that extend the Gson library.
 */
package org.sw4j.gson.adapter;
